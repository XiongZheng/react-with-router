import React from 'react';
import {Redirect} from 'react-router';

const Goods = () => {
  return (
    <Redirect to="/products" />
  )
};

export default Goods;
