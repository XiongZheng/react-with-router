import React, {Component} from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route} from 'react-router';
import '../styles/App.css';
import Header from './Header';
import Home from './Home';
import Profile from './Profile';
import About from './About';
import Products from './Products';
import Goods from './Goods';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <Header />
          <Route path="/home" exact component={Home} />
          <Route path="/profile" exact component={Profile} />
          <Route path="/about" exact component={About} />
          <Route path="/products" exact component={Products} />
          <Route path="/goods" exact component={Goods} />
          <Route component={Home} />
        </Router>
      </div>
    );
  }
}

export default App;
